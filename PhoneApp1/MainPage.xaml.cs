﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PhoneApp1.Resources;
using System.Windows.Media.Imaging;
using Microsoft.Xna.Framework;
using System.Windows.Media;

namespace PhoneApp1
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void RotateApply_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                image1.RenderTransform = new RotateTransform()
                {
                    CenterX = image1.ActualWidth / 2,
                    CenterY = image1.ActualHeight / 2,
                    Angle = Convert.ToDouble(degree_txtbx.Text)
                };
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message + "\n(Please enter a number for degree)");
            }
        }

        private void txtbx_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Text = "";
        }

        private void CropApply_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] coordsStr = crop_txtbx.Text.Split(',');
                int length = coordsStr.Length;
                if (length != 4)
                {
                    throw new ArgumentException();
                }
                double[] coords = new double[length];
                int i;
                for (i = 0; i < length; i++)
                {
                    coords[i] = Convert.ToDouble(coordsStr[i]);
                }
                WriteableBitmap wb = new WriteableBitmap((int)(coords[2] - coords[0]), (int)(coords[3] - coords[1]));
                wb.Render(image1, new TranslateTransform { X = -coords[0], Y = -coords[1] });
                wb.Invalidate();
                image1.Source = wb;
                image1.Width = coords[2] - coords[0];
                image1.Height = coords[3] - coords[1];
                image1.RenderTransform = new TranslateTransform()
                {
                    X = coords[0],
                    Y = coords[1]
                };
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message + "\n(Please enter 4 numbers for coordinates)");
            }
        }

        private void ResizeApply_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] coordsStr = resize_txtbx.Text.Split(',');
                int length = coordsStr.Length;
                if (length != 2)
                {
                    throw new ArgumentException();
                }
                double[] coords = new double[length];
                int i;
                for (i = 0; i < length; i++)
                {
                    coords[i] = Convert.ToDouble(coordsStr[i]);
                }
                image1.Height = coords[0];
                image1.Width = coords[1];
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message + "\n(Please enter 2 numbers for coordinates)");
            }
        }

        private void ImageSourceApply_btn_Click(object sender, RoutedEventArgs e)
        {
            BitmapImage newImg = new BitmapImage(new Uri(imageSource_txtbx.Text));
            image1.Source = newImg;
            image1.Width = newImg.PixelWidth;
            image1.Height = newImg.PixelHeight;
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}